# ERP+MES+甘特图排产

 **最近工作比较忙，等闲下来就会梳理出来，上传。** 

#### 介绍
本系统糅合了ERP和MES的功能；支持从订单 -> 排单 -> 生产跟踪 -> 装配 -> 发货，一系列的业务流程闭环操作。

#### 软件架构

![输入图片说明](https://foruda.gitee.com/images/1662139028705236066/1cb6bf50_1660800.png "业务流程图.png")
![输入图片说明](cdn%E5%B9%BB%E7%81%AF%E7%89%875.PNG)
![输入图片说明](cdn%E5%B9%BB%E7%81%AF%E7%89%877.PNG)
![输入图片说明](cdn%E5%B9%BB%E7%81%AF%E7%89%878.PNG)
![输入图片说明](cdn%E5%B9%BB%E7%81%AF%E7%89%879.PNG)
![输入图片说明](cdn%E5%B9%BB%E7%81%AF%E7%89%8710.PNG)
![输入图片说明](cdn%E5%B9%BB%E7%81%AF%E7%89%8711.PNG)
![输入图片说明](cdn%E5%B9%BB%E7%81%AF%E7%89%8712.PNG)
![输入图片说明](cdn%E5%B9%BB%E7%81%AF%E7%89%8713.PNG)

#### 技术栈

后端：采用.NET Core平台，ASP.NET Core3.1，C#语言（使用反射等高级技术），Entity FrameworkCore（数据库ORM框架）。
前端：Vue2.x全家桶+Ant Design Vue，其中集成常用组件，力求方便项目开发。

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
